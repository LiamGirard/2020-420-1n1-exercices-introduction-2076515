package ca.cegepdrummond;

import java.sql.SQLOutput;

public class Serie2_Variables {
    /*
     * Complétez le code suivant afin de déclarer la
     * variable "varA" comme étant du type int
     * et ayant la valeur 1234
     */
    public void variable1() {
        int varA = 1234;
        System.out.println(varA);



}

    /*
      Assignez des valeurs aux variables a,b,c

     * b doit avoir la valeur 4
     * c doit avoir la valeur 5
     *
     * Le println doit afficher  3 4 5
     */
    public void variable2() {

        int A = 3 ;
        int B = 4 ;
        int C = 5 ;
        System.out.println("3 4 5" );

    }


        /* enlever cette ligne de commentaire
        int a, b;
        int c;

        System.out.println(a + " " + b + " " + c );
        enlever cette ligne de commentaire */


    /*
     * Ajoutez le nombre minimal de parenthèses () pour que ce code affiche 42
     */
    public void variable3() {

        int a = (42 + 3 + 4 - 7) * 1 ;
        System.out.println( a );

    }

    /*
     * corrigez le code pour qu'il affiche le caractère 'a'
     */
    public void variable4() {
        String X = "a" ;
        System.out.println(X);
    }



}
